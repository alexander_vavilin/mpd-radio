# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyKDE4.kio import *
from PyKDE4.kdeui import *
from PyKDE4.kdecore import *
from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript
from PyKDE4.solid import Solid
import json
import os
import mpd
from os.path import expanduser


class MPDRadioButton(Plasma.ToolButton):
	url = ""

class MPDRadioPlasmoid(plasmascript.Applet):
    def __init__(self, parent, args=None):
        plasmascript.Applet.__init__(self, parent)
            
    def init(self):
        """Applet settings"""

        
        self.setHasConfigurationInterface(True)
        self.setAspectRatioMode(Plasma.Square)

        self.theme = Plasma.Svg(self)
        self.theme.setImagePath("widgets/background")
        self.setBackgroundHints(Plasma.Applet.DefaultBackground)

        self.layout = QGraphicsLinearLayout(Qt.Vertical, self.applet)
            
        json_data = open(expanduser("~")+'/.radio-mpd/settings.json','r')
        data = json.load(json_data)
        json_data.close()

        self.label = Plasma.Label(self.applet)
        self.label.setText("Now playing:")
        self.layout.addItem(self.label)

        self.songtitlelabel = Plasma.Label(self.applet)

        
        self.layout.addItem(self.songtitlelabel)


        for i in data['stations']:
        	self.button = MPDRadioButton(self.applet)

        	self.button.setText("{0}".format(i['name']))
        	self.button.clicked.connect(self.handleButton)
        	self.button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        	self.button.url = i['url']
        	self.layout.addItem(self.button)

        self.applet.setLayout(self.layout)
        self.resize(200, len(data['stations'])*50)
        self.timer = QTimer()
        self.connect(self.timer, SIGNAL("timeout()"), self.getSongTitle)
        self.timer.start(1000)
        self.client = mpd.MPDClient()
        self.client.connect("localhost", 6600)
        self.getSongTitle()


    def handleButton(self):
        #print(self.sender().url)
        
        #print(self.client.currentsong()['title'])
        self.client.stop()
        self.client.clear()
        self.client.add(self.sender().url)
        self.client.play(0)
    
    def getSongTitle(self):
        try:                       
            self.songtitlelabel.setText(unicode(self.client.currentsong()['title'],'utf8'))
        except:
            self.songtitlelabel.setText('')

def CreateApplet(parent):
    return MPDRadioPlasmoid(parent)