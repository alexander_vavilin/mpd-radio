#!/usr/bin/python3
import mpd

# use_unicode will enable the utf-8 mode for python2
# see http://pythonhosted.org/python-mpd2/topics/advanced.html#unicode-handling
# use_unicode=True
client = mpd.MPDClient()
client.connect("localhost", 6600)

#for entry in client.lsinfo("/"):
    #print("%s" % entry)
print(unicode(client.currentsong()['title'],'utf8'))

#for key, value in client.currentsong():
 #   print("%s: %s" % (key, value))